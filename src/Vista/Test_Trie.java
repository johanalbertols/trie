/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.io.IOException;
import Clases.ArbolTrie;
import texto.ArchivoLeerURL;

/**
 *
 * @author Cristian
 */
public class Test_Trie {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        
        ArbolTrie a = new ArbolTrie();
        ArchivoLeerURL arch = new ArchivoLeerURL("http://madarme.co/persistencia/palabras.txt");
        Object o[] = arch.leerArchivo();
        
        for (int i = 0; i < o.length; i++) {
            
            String linea = (String)o[i];
            linea = linea.replaceAll(" ", "");
            String palabras[] = linea.split(",");
            for (int j = 0; j < palabras.length; j++) {
                System.out.println(palabras[j]);
                a.agregar(palabras[j]);
            }
        }
            
    }
    
    
    
}
